package com.example.eceb.models;

public class BabiesModel {
    String parentName;
    String time;
    String gender;
    String location;
    String currentStatus; // normal, problem, danger
    String lastStatus;
    String message;

    public BabiesModel(String parentName, String time, String gender, String location, String currentStatus, String lastStatus, String message) {
        this.parentName = parentName;
        this.time = time;
        this.gender = gender;
        this.location = location;
        this.currentStatus = currentStatus;
        this.lastStatus = lastStatus;
        this.message = message;
    }

    public BabiesModel(String parentName, String time, String gender, String location, String currentStatus) {
        this.parentName = parentName;
        this.time = time;
        this.gender = gender;
        this.location = location;
        this.currentStatus = currentStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(String lastStatus) {
        this.lastStatus = lastStatus;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
