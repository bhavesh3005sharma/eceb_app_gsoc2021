package com.example.eceb.models;

public class DoctorsModel {
    String status;

    String name;

    String photoUri;

    public DoctorsModel(String status, String name, String photoUri) {
        this.status = status;
        this.name = name;
        this.photoUri = photoUri;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }
}
