package com.example.eceb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eceb.R;
import com.example.eceb.models.DoctorsModel;

import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DoctorsAdapter extends RecyclerView.Adapter<DoctorsAdapter.viewHolder> {

    ArrayList<DoctorsModel> list;
    Context context;
    adapterListener mListener;

    public DoctorsAdapter(ArrayList<DoctorsModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void setupListener(adapterListener mListener) {
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.doctors_call_layout, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        DoctorsModel data = list.get(position);
        holder.textName.setText(data.getName());
        holder.textStatus.setText(data.getStatus());
        if (data.getPhotoUri().equals("1")){
            holder.doctorsPhoto.setImageResource(R.drawable.doctor1);
        }else  if (data.getPhotoUri().equals("2")){
            holder.doctorsPhoto.setImageResource(R.drawable.doctor2);
        }else {
            holder.doctorsPhoto.setImageResource(R.drawable.doctor3);
        }
    }

    @Override
    public int getItemCount() {
        return(list!=null? list.size() : 0);
    }

    public class viewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.doctorsPhoto)
        ImageView doctorsPhoto;
        @BindView(R.id.textName)
        TextView textName;
        @BindView(R.id.textStatus)
        TextView textStatus;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.itemClicked(getAdapterPosition());
                }
            });
        }
    }

    public interface adapterListener{
        void itemClicked(int position);
    }


}
