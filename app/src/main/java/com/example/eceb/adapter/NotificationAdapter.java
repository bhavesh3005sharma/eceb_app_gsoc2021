package com.example.eceb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eceb.R;
import com.example.eceb.models.BabiesModel;
import com.google.android.material.circularreveal.cardview.CircularRevealCardView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.viewHolder> {

    ArrayList<BabiesModel> list;
    Context context;
    int notificationType = 1;

    public NotificationAdapter(ArrayList<BabiesModel> list, Context context, int notificationType) {
        this.list = list;
        this.context = context;
        this.notificationType = notificationType;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_notification, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        BabiesModel data = list.get(position);
        holder.textName.setText("Baby of " + data.getParentName());
        holder.textTime.setText("EAT " + data.getTime());
        holder.textLocation.setText("Location : " + data.getLocation());

        if (data.getCurrentStatus().equals("Normal")) {
            holder.cardStatusColor.setCardBackgroundColor(context.getResources().getColor(R.color.green));
        } else if (data.getCurrentStatus().equals("Problem")) {
            holder.cardStatusColor.setCardBackgroundColor(context.getResources().getColor(R.color.yellow));
        } else {
            // Baby is in danger
            holder.cardStatusColor.setCardBackgroundColor(context.getResources().getColor(R.color.red));
        }

        if (notificationType == 1) {
            holder.textStatus.setText("Status Changes : \n" + data.getLastStatus() + " to " + data.getCurrentStatus());
        } else {
            holder.textStatus.setText(data.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return (list != null ? list.size() : 0);
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textName)
        TextView textName;
        @BindView(R.id.textLocation)
        TextView textLocation;
        @BindView(R.id.textTime)
        TextView textTime;
        @BindView(R.id.textStatus)
        TextView textStatus;
        @BindView(R.id.cardStatusColor)
        CircularRevealCardView cardStatusColor;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
