package com.example.eceb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eceb.R;
import com.example.eceb.models.BabiesModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BabiesAdapter extends RecyclerView.Adapter<BabiesAdapter.viewHolder> {

    ArrayList<BabiesModel> list;
    Context context;

    public BabiesAdapter(ArrayList<BabiesModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_baby, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        BabiesModel data = list.get(position);
        holder.textName.setText("Baby of " + data.getParentName());
        holder.textTime.setText(data.getTime() + " minutes from Birth");
        holder.textGender.setText(data.getGender());
        holder.textLocation.setText("Location : " + data.getLocation());
        if (data.getGender().equals("Male"))
            holder.imageGender.setImageResource(R.drawable.male_icon);
        else
            holder.imageGender.setImageResource(R.drawable.female_icon);

        if (data.getCurrentStatus().equals("Normal")) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.green));
            holder.textTime.setBackgroundColor(context.getResources().getColor(R.color.green));
        } else if (data.getCurrentStatus().equals("Problem")) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.yellow));
            holder.textTime.setBackgroundColor(context.getResources().getColor(R.color.yellow));
        } else {
            // Baby is in danger
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.red));
            holder.textTime.setBackgroundColor(context.getResources().getColor(R.color.red));
        }
    }

    @Override
    public int getItemCount() {
        return (list != null ? list.size() : 0);
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textName)
        TextView textName;
        @BindView(R.id.textLocation)
        TextView textLocation;
        @BindView(R.id.textTime)
        TextView textTime;
        @BindView(R.id.textGender)
        TextView textGender;
        @BindView(R.id.imageGender)
        ImageView imageGender;
        @BindView(R.id.cardLayout)
        ConstraintLayout cardLayout;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
