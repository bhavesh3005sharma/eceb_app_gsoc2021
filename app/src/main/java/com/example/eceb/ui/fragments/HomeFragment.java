package com.example.eceb.ui.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.eceb.R;
import com.example.eceb.adapter.DoctorsAdapter;
import com.example.eceb.models.DoctorsModel;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HomeFragment extends Fragment implements DoctorsAdapter.adapterListener{
    @BindView(R.id.recyclerViewDoctors)
    RecyclerView recyclerView;
    @BindView(R.id.cardRegisterBaby)
    MaterialCardView cardRegisterBaby;
    @BindView(R.id.layoutDoctorCalled)
    LinearLayout layoutDoctorCalled;
    @BindView(R.id.imageDoctorCalled)
    ImageView imageDoctorCalled;
    @BindView(R.id.textDoctorCalledName)
    TextView textDoctorCalledName;

    Unbinder unbinder;
    ArrayList<DoctorsModel> list = new ArrayList<>();
    DoctorsAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        // bind the views
        unbinder = ButterKnife.bind(this,view);

        // get Data from server
        getData();

        // update data in ui
        setupUi();

        return view;
    }

    private void setupUi() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.hasFixedSize();
        adapter = new DoctorsAdapter( list,getContext());
        recyclerView.setAdapter(adapter);
        adapter.setupListener(this);
    }

    private void getData() {
        // since no api present populate dummy data

        list = new ArrayList<>();
        list.add(new DoctorsModel("Online","Andrea","1"));
        list.add(new DoctorsModel("Online","Kim","2"));
        list.add(new DoctorsModel("Online","Jane","3"));
        list.add(new DoctorsModel("Online","Takashi","1"));
        list.add(new DoctorsModel("Online","Karima","2"));
        list.add(new DoctorsModel("Online","Suhan","3"));

    }

    @Override
    public void itemClicked(int position) {
        layoutDoctorCalled.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        textDoctorCalledName.setText(list.get(position).getName()+" has been alerted");
        if (list.get(position).getPhotoUri().equals("1")){
            imageDoctorCalled.setImageResource(R.drawable.doctor1);
        }else  if (list.get(position).getPhotoUri().equals("2")){
            imageDoctorCalled.setImageResource(R.drawable.doctor2);
        }else {
            imageDoctorCalled.setImageResource(R.drawable.doctor3);
        }
    }
}