package com.example.eceb.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.eceb.R;
import com.example.eceb.adapter.PageViewAdapter;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NotificationsFragment extends Fragment {
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    Unbinder unbinder;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null)
            unbinder.unbind();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);
        // bind the views
        unbinder = ButterKnife.bind(this, view);

        // setup view Pager
        setUpViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    public void setUpViewPager(ViewPager viewPager) {
        PageViewAdapter pageViewAdapter = new PageViewAdapter(getChildFragmentManager(), 0);
        RiskAssessmentFragment riskAssessmentFragment = new RiskAssessmentFragment();
        MonitoringAlertsFragment monitoringAlertsFragment = new MonitoringAlertsFragment();

        pageViewAdapter.addFragment(riskAssessmentFragment, "Risk Assessment");
        pageViewAdapter.addFragment(monitoringAlertsFragment, "Monitoring Alerts");
        viewPager.setAdapter(pageViewAdapter);
    }
}