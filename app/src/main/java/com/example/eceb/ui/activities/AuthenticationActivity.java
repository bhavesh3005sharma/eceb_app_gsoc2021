package com.example.eceb.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.eceb.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AuthenticationActivity extends AppCompatActivity {
    @BindView(R.id.editTextId)
    EditText editTextId;
    @BindView(R.id.editTextPassword)
    EditText editTextPassword;
    @BindView(R.id.buttonSignIn)
    TextView buttonSignIn;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    Unbinder unbinder;
    String authType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        // bind the views
        unbinder = ButterKnife.bind(this);

        // getUserType
        authType = getIntent().getStringExtra("authType");

        setupUi();

        // check on sign button clicks
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // read inputs by user
                String id = editTextId.getText().toString().trim();
                String password = editTextPassword.getText().toString().trim();

                // check for valid data
                if(!isValid(id,password)) return;

                // showProgressBar
                progressBar.setVisibility(View.VISIBLE);

                // data is valid now match id and password with server
                // since we don't have api call now proceed to next activity after 2 sec wait.
                // TODO : Api Call to Authenticate User
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // hideProgressBar
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(AuthenticationActivity.this, "Login Success!!", Toast.LENGTH_SHORT).show();

                        // redirect to homepage
                        Intent intent = new Intent(AuthenticationActivity.this,HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                },2000);
            }
        });
    }

    private void setupUi() {
        if(authType!=null && authType.equals("facility")){
            editTextId.setHint("Enter the Employee Id");
        }
    }

    private boolean isValid(String id, String password) {
        if(id.isEmpty()){
            editTextId.setError("Id is Required");
            editTextId.requestFocus();
            return false;
        }

        if(password.isEmpty()){
            editTextPassword.setError("Password is Required");
            editTextPassword.requestFocus();
            return false;
        }

        return true;
    }
}