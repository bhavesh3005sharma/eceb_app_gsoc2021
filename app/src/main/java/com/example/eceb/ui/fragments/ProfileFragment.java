package com.example.eceb.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eceb.R;
import com.example.eceb.adapter.ActivitiesAdapter;
import com.example.eceb.adapter.DoctorsAdapter;
import com.example.eceb.models.ActivityModel;
import com.example.eceb.models.DoctorsModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ProfileFragment extends Fragment {
    @BindView(R.id.recyclerViewActivities)
    RecyclerView recyclerView;

    Unbinder unbinder;
    ArrayList<ActivityModel> list = new ArrayList<>();
    ActivitiesAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        // bind the views
        unbinder = ButterKnife.bind(this,view);

        // get Data from server
        getData();

        // update data in ui
        setupUi();

        return view;
    }

    private void setupUi() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.hasFixedSize();
        adapter = new ActivitiesAdapter( list,getContext());
        recyclerView.setAdapter(adapter);
    }

    private void getData() {
        // since no api present populate dummy data

        list = new ArrayList<>();
        list.add(new ActivityModel("Baby 1 of Minia registered at Post Natal Ward at 11:56 AM","22/3/21","11:45 AM"));
        list.add(new ActivityModel("Baby 1 of Anisha registered at Post Natal Ward at 11:56 AM","25/3/21","11:45 AM"));
        list.add(new ActivityModel("Baby 1 of Sikaga registered at Post Natal Ward at 11:56 AM","22/3/21","11:45 AM"));
        list.add(new ActivityModel("Baby 1 of Sufia registered at Post Natal Ward at 11:56 AM","28/3/21","11:45 AM"));
        list.add(new ActivityModel("Baby 1 of Minia registered at Post Natal Ward at 11:56 AM","22/3/21","11:45 AM"));
        list.add(new ActivityModel("Baby 1 of Minia registered at Post Natal Ward at 11:56 AM","22/2/21","11:45 AM"));
        list.add(new ActivityModel("Baby 1 of Minia registered at Post Natal Ward at 11:56 AM","22/2/21","11:45 AM"));
        list.add(new ActivityModel("Baby 1 of Minia registered at Post Natal Ward at 11:56 AM","22/2/21","11:45 AM"));
        list.add(new ActivityModel("Baby 1 of Minia registered at Post Natal Ward at 11:56 AM","22/2/21","11:45 AM"));

    }
}