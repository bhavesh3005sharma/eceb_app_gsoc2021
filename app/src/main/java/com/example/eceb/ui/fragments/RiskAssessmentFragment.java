package com.example.eceb.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eceb.R;
import com.example.eceb.adapter.NotificationAdapter;
import com.example.eceb.models.BabiesModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RiskAssessmentFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    Unbinder unbinder;
    ArrayList<BabiesModel> list = new ArrayList<>();
    NotificationAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_risk_assesment, container, false);

        // bind the views
        unbinder = ButterKnife.bind(this, view);

        // get Data from server
        getData();

        // update data in ui
        setupUi();

        return view;
    }

    private void setupUi() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.hasFixedSize();
        adapter = new NotificationAdapter(list, getContext(), 1);
        recyclerView.setAdapter(adapter);
    }

    private void getData() {
        // since no api present populate dummy data

        list = new ArrayList<>();
        list.add(new BabiesModel("Nia", "14", "Male", "Ward 1", "Normal", "Problem", "25-30 minutes checkup"));
        list.add(new BabiesModel("Saki", "14", "Female", "Ward 1", "Problem", "Danger", "17 minutes checkup"));
        list.add(new BabiesModel("Shivahi", "14", "Male", "Ward 1", "Normal", "Problem", "25-30 minutes checkup"));
        list.add(new BabiesModel("Rui", "14", "Male", "Ward 1", "Danger", "Problem", "25-30 minutes checkup"));
        list.add(new BabiesModel("Pihu", "14", "Female", "Ward 1", "Normal", "Problem", "20 minutes checkup"));
        list.add(new BabiesModel("Nia", "14", "Male", "Ward 1", "Problem", "Normal", "45 minutes checkup"));
        list.add(new BabiesModel("Nia", "14", "Female", "Ward 1", "Normal", "Problem", "25-30 minutes checkup"));
        list.add(new BabiesModel("Nia", "14", "Male", "Ward 1", "Danger", "Problem", "25-30 minutes checkup"));
        list.add(new BabiesModel("Nia", "14", "Female", "Ward 1", "Problem", "Danger", "30 minutes checkup"));
        list.add(new BabiesModel("Nia", "14", "Male", "Ward 1", "Normal", "Problem", "25-30 minutes checkup"));
    }
}