package com.example.eceb.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eceb.R;
import com.example.eceb.adapter.BabiesAdapter;
import com.example.eceb.models.BabiesModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ListOfBabiesFragment extends Fragment {
    @BindView(R.id.recyclerViewBabies)
    RecyclerView recyclerView;

    Unbinder unbinder;
    ArrayList<BabiesModel> list = new ArrayList<>();
    BabiesAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_babies, container, false);

        // bind the views
        unbinder = ButterKnife.bind(this, view);

        // get Data from server
        getData();

        // update data in ui
        setupUi();

        return view;
    }

    private void setupUi() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.hasFixedSize();
        adapter = new BabiesAdapter(list, getContext());
        recyclerView.setAdapter(adapter);
    }

    private void getData() {
        // since no api present populate dummy data

        list = new ArrayList<>();
        list.add(new BabiesModel("Nia", "14", "Male", "Ward 1", "Normal"));
        list.add(new BabiesModel("Saki", "14", "Female", "Ward 1", "Problem"));
        list.add(new BabiesModel("Shivahi", "14", "Male", "Ward 1", "Normal"));
        list.add(new BabiesModel("Rui", "14", "Male", "Ward 1", "Danger"));
        list.add(new BabiesModel("Pihu", "14", "Female", "Ward 1", "Normal"));
        list.add(new BabiesModel("Nia", "14", "Male", "Ward 1", "Problem"));
        list.add(new BabiesModel("Nia", "14", "Female", "Ward 1", "Normal"));
        list.add(new BabiesModel("Nia", "14", "Male", "Ward 1", "Danger"));
        list.add(new BabiesModel("Nia", "14", "Female", "Ward 1", "Problem"));
        list.add(new BabiesModel("Nia", "14", "Male", "Ward 1", "Normal"));


    }
}