package com.example.eceb.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.eceb.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.textIndividual)
    TextView textIndividual;
    @BindView(R.id.textFacility)
    TextView textFacility;
    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // bind the views
        unbinder = ButterKnife.bind(this);

        // setClickListener
        textIndividual.setOnClickListener(this);
        textFacility.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this,AuthenticationActivity.class);
        if (view.getId() == R.id.textIndividual) {
            intent.putExtra("authType", "individual");
        }else{
            intent.putExtra("authType", "facility");
        }

        startActivity(intent);
        finish();
    }
}