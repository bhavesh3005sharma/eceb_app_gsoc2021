# ECEB_App_Gsoc2021

         **Essential Care for Every Baby (ECEB)**

         ![ECEB](https://gitlab.com/bhavesh3005sharma/eceb_app_gsoc2021/-/blob/master/app/src/main/res/drawable-hdpi/app_logo.png)

The Essential Care for Every Baby (ECEB) educational and training program, developed by the American Academy of Pediatrics, provides knowledge, skills, and competencies to nurses and doctors in low/middle-income settings so that they can provide life-saving care to newborns from birth through 24 hours postnatal.

This is Baic app as a starter task of Libra Health for a project of ECEB 
> Apk Link Of the App : [APK Link](https://gitlab.com/bhavesh3005sharma/eceb_app_gsoc2021/-/blob/master/app_apk/ECEB.apk)
(This app can be downloaded and installed on any android device and you can check it)

> Screenshots of the App : 
![Welcome Screen](https://gitlab.com/bhavesh3005sharma/eceb_app_gsoc2021/-/blob/master/app_screens/1.%20welcome_screen.jpeg)
![Authentication Screen-1](https://gitlab.com/bhavesh3005sharma/eceb_app_gsoc2021/-/blob/master/app_screens/2.%20auth_screen1.jpeg)
![Authentication Screen-2](https://gitlab.com/bhavesh3005sharma/eceb_app_gsoc2021/-/blob/master/app_screens/3.%20auth_screen2.jpeg)
![Home Page-1](https://gitlab.com/bhavesh3005sharma/eceb_app_gsoc2021/-/blob/master/app_screens/4.%20home_page1.jpeg)
![Home Page-2](https://gitlab.com/bhavesh3005sharma/eceb_app_gsoc2021/-/blob/master/app_screens/5.%20home_page2.jpeg)
![List of Babies Screen](https://gitlab.com/bhavesh3005sharma/eceb_app_gsoc2021/-/blob/master/app_screens/6.%20list_of_babies.jpeg)
![Notification Screen-1](https://gitlab.com/bhavesh3005sharma/eceb_app_gsoc2021/-/blob/master/app_screens/7.%20notification_risk_assessment.png)
![Notification Screen-2](https://gitlab.com/bhavesh3005sharma/eceb_app_gsoc2021/-/blob/master/app_screens/8.%20notification_monitoring_alerts.png)
![Profile Page-1](https://gitlab.com/bhavesh3005sharma/eceb_app_gsoc2021/-/blob/master/app_screens/9.%20profile_page1.jpeg)
![Profile Page-2](https://gitlab.com/bhavesh3005sharma/eceb_app_gsoc2021/-/blob/master/app_screens/10.%20profile_page2.jpeg)

